# Chatbot-socket.io  
  Use the package manager [npm](https://www.npmjs.com/) to install socket.io, socket.io-client, and express.  
> nvm install socket.io 
 
> nvm i socket.io-client 
  Use node v14 use : 
> nvm install 14 
 ## Configuration Replace in /server/server.js your localhost port for running application :  
> const io = new Server(server, {   cors: {     origin: 'localhost',     methods: ['GET', 'POST']   } }); 
  Replace in /src/Chat.js and /src/index.js your localhost port to connect you to your server :  
> const io = require('socket.io-client');  const socket = io.connect('http://127.0.0.1:3000'); socket.on('connection', () => {   // eslint-disable-next-line no-console   console.log(socket.id); }); 
 ### Usage  Start the application dev with :  
> npm run serve
 Start the server application i
